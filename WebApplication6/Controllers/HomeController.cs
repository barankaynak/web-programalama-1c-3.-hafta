﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication6.Models;

namespace WebApplication6.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Data.NorthwindEntities db = new Data.NorthwindEntities();
            //var sorgu = db.Employees
            //    .Where(x => x.FirstName.Contains("A"))
            //    .OrderBy(x => x.FirstName)
            //    .ThenBy(x => x.LastName)
            //    .Select(emp => new Calisan
            //    {
            //        ID = emp.EmployeeID,
            //        TamAd = emp.FirstName + " " + emp.LastName
            //    });
            //var sorgu = from order in db.Orders
            //            join emp in db.Employees on order.EmployeeID equals emp.EmployeeID
            //            select new { order.OrderID, emp.FirstName, emp.LastName };

            //var sorgu = db.Orders
            //    .Select(order => new { order.OrderID, order.Employees.FirstName, order.Employees.LastName });

            //var sql = sorgu.ToString();

            //var model = sorgu.ToList();
            var model = db.Customers.Take(5).ToList();

            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            Data.NorthwindEntities db = new Data.NorthwindEntities();
            var model = db.Employees.FirstOrDefault(x => x.Country == "TURKEY");


            return View(model);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}